;; This is an operating system configuration template
;; to test containers via guix system (also on foregin distros)
;; see https://www.gnu.org/software/guix/manual/en/html_node/Invoking-guix-system.html

(use-modules (gnu))
(use-modules (rnrs lists))
(use-service-modules networking ssh)
(use-package-modules screen ssh emacs)

(operating-system
  (host-name "test-container")
  (timezone "Europe/Rome")
  (locale "en_US.utf8")

  ;; Boot in "legacy" BIOS mode
  (bootloader (bootloader-configuration
                (bootloader grub-bootloader)
                (target "/dev/sda")))

  (file-systems (cons (file-system
                        (device (file-system-label "guixsd"))
                        (mount-point "/")
                        (type "ext4"))
                      %base-file-systems))
  
  ;; This is where user accounts are specified.  The "root"
  ;; account is implicit, and is initially created with the
  ;; empty password.
  (users (cons (user-account
                (name "gbiscuolo")
                (comment "Giovanni Biscuolo (admin)")
                (group "users")
                (supplementary-groups '("wheel" "audio" "video"))
                (home-directory "/home/gbiscuolo"))
               %base-user-accounts))
  
  ;; Globally-installed packages.
  (packages (cons* screen openssh emacs %base-packages))

  ;; Add services to the baseline: a DHCP client and
  ;; an SSH server.
  (services (cons* (service dhcp-client-service-type)
		   (service docker-service-type)
		   (service openssh-service-type
                            (openssh-configuration
			     (x11-forwarding? #t)
                             (permit-root-login 'without-password)
                             (authorized-keys
                              `(("gbiscuolo" ,(local-file "ssh-authorized-keys/gbiscuolo.pub"))))
                             (port-number 22)))
		   (remove (lambda (service)
			     (eq? (service-kind service) mingetty-service-type)
			     (eq? (service-kind service) console-font-service-type))
			   %base-services))))

