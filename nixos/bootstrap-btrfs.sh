#!/bin/sh

device="/dev/sda"

# simple installation using btrfs using the whole disk device
# and subvolumes for root, var, home and tmp
echo "Bootstrapping on ${device} as BTRFS device"

# partitioning with a MBR partition table
parted ${device} -- mklabel msdos
parted ${device} -- mkpart primary 1MiB -2GiB
parted ${device} -- mkpart primary linux-swap -2GiB 100%  # 2GB swap space

# format second partition as swap
mkswap -L swap ${device}2
swapon ${device}2

# format first partition as BTRFS and mount it
mkfs.btrfs -f -L nixos ${device}1
mount -t btrfs ${device}1 /mnt/

# create OS subvolume, creatively called nixos
btrfs subvolume create /mnt/nixos
umount /mnt/

# remount nixos subvolume and create dedicated subvolumes to: var, home, tmp
mount -t btrfs -o subvol=nixos ${device}1 /mnt/
btrfs subvolume create /mnt/var
btrfs subvolume create /mnt/home
btrfs subvolume create /mnt/tmp

# let's finally generate the basic nixos config
nixos-generate-config --root /mnt/

# maintain generated config as .orig, use teh config shipped in this repo
mv /mnt/etc/nixos/configuration.nix /mnt/etc/nixos/configuration.nix.orig
cp configuration.nix /mnt/etc/nixos/configuration.nix
cp ssh-keys.nix /mnt/etc/nixos

# disabled: for now you must do this explicitly
#nixos-install
#reboot
