#!/bin/sh

device="/dev/sda"

echo "Partitioning ${device} with BTRFS for GuixSD install"

# partitioning with a MBR partition table
# first partition for BTRFS, second for swap
parted ${device} -- mklabel msdos
parted ${device} -- mkpart primary 1MiB -2GiB
parted ${device} -- mkpart primary linux-swap -2GiB 100%  # 2GB swap space

# format second partition as swap
mkswap -L swap ${device}2
swapon ${device}2

# format first partition as BTRFS and mount it
mkfs.btrfs -f -L guixsd ${device}1
mount -t btrfs ${device}1 /mnt/

# create guixsd subvolume
btrfs subvolume create /mnt/guixsd
umount /mnt/

# create dedicated subvolumes for var, home and tmp
mount -t btrfs -o subvol=guixsd ${device}1 /mnt/
btrfs subvolume create /mnt/var
btrfs subvolume create /mnt/home
btrfs subvolume create /mnt/tmp
