#!/bin/sh

# WARNING: LVM still not supported by GuixSD
# see https://www.gnu.org/software/guix/manual/en/html_node/Limitations.html#Limitations

device="/dev/sda"

echo "Partitioning ${device} with ext4 on LVM for GuixSD install"

# partitioning with a MBR partition table
parted ${device} -- mklabel msdos
parted ${device} -- mkpart primary 1MiB -2GiB
parted ${device} -- mkpart primary linux-swap -2GiB 100%  # 2GB swap space

# format second partition as swap
mkswap -L swap ${device}2
swapon ${device}2

# create physical and logical volumes
pvremove -ffy ${device}1
pvcreate -f ${device}1
vgcreate vg0 ${device}1
lvcreate --name guixsd -L 10G vg0

# format destination volume as etx4 FS and mount it
mkfs.ext4 -L guixsd /dev/vg0/guixsd
mount -t ext4 /dev/vg0/guixsd /mnt/
