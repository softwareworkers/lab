{ config, pkgs, ...}:

{
  imports = [
    ./hardware-configuration.nix
  ];

  boot = {
    loader = {
      grub.device = "/dev/sda";
      grub.enable = true;
      grub.version = 2;
      grub.extraConfig = ''
          serial --speed=19200 --unit=0 --word=8 --parity=no --stop=1;
	  terminal_input serial;
	  terminal_output serial
	  '';
    };
    kernelParams = [ "console=ttyS0,19200n8" ];
  };

  environment = {
    systemPackages = with pkgs; [
      curl
      gcc
      git
      screen
      vim
      wget
    ];

    variables = {
      EDITOR = "vim";
    };
  };

  i18n = {
    defaultLocale = "en_US.UTF-8";
    consoleKeyMap = "it";
  };

  networking = {
    hostName = "nixos-test";
  };

  programs = {
    bash = {
      enableCompletion = true;
    };

    ssh = {
      startAgent = true;
    };
  };

  services = {
    openssh = {
      enable = true;
      permitRootLogin = "prohibit-password";
    };
  };

  time.timeZone = "Europe/Rome";

  users = {
    extraUsers = {
      gbiscuolo = {
        extraGroups = [ "wheel" ];
        isNormalUser = true;
        openssh.authorizedKeys.keys = with import ./ssh-keys.nix; [ gbiscuolo ];
      };

      root = {
        openssh.authorizedKeys.keys = with import ./ssh-keys.nix; [ gbiscuolo ];
      };
    };
  };

  system = {
   stateVersion = "18.09";
   autoUpgrade.enable = true;
  };
  
}