# (Simple) Bootstrap of GuixSD #

This is a little project to showcase how to bootstrap a [GuixSD distribution](https://www.gnu.org/software/guix/) based machine, with script to test installation using different filesystems (and volume managers) on a *single disk* (no RAID).

Prerequisites to use this howto:

- know how to burn a CD or prepare a bootable USB key
- know how to boot from installation media
- suggested: know how to create and boot a KVM virtual machine, connected to a network bridge and getting IP via a dhcp service
- suggested: know how to manage GNU screen sessions

## How to use ##

### Set up your test virtual machine ##

If you are testing on bare metal you can skip to the next section.

Probably the best way to test GuixSD is to [use a dedicated GuixSD QEMU/KVM virtual machine](https://www.gnu.org/software/guix/manual/en/html_node/Installing-GuixSD-in-a-VM.html#Installing-GuixSD-in-a-VM); for this purpose I'm using [`libvirt` and its `virsh` CLI](https://libvirt.org/virshcmdref.html) on a Debian/Stretch machine to manage definition, start/stop and decommissioning of my test machines in a dedicated virtual network (also managed via `virsh`).

### Boot and prepare for (remote) installation ###

Boot your (virtual) machine using the latest installation CD/USB you can find on [GuixSD Download page](https://www.gnu.org/software/guix/download/) and connect to its console (e.g. via VNC).

[Prepare for installation](https://www.gnu.org/software/guix/manual/en/html_node/Preparing-for-Installation.html#Preparing-for-Installation), in particular:

    loadkeys it # use your keyboard layout
	passwd # set root password (for remote access)
	ip a # find the network interface
	ifconfig <interface> up # activate the interface
	dhclient -v <interface> # get IP via DHCP
	herd start ssh-daemon
	guix package -i screen
	
You can follow the rest of the installation process via ssh, I **strongly suggest** using a GNU screen session.

### Install GuixSD on host device ###

**WARNING**: be sure to replace the ssh public keys saved in ``ssh-authorized-keys`` folder and used in ``configuration/*.scm`` configurations with *yours*.

1. customize the ``device`` variable in ``partition*.sh``, use ``lsblk`` to list available devices.

1. partition the ``device`` using your preferred ``partition-*.sh`` method

1. instantiate GuixSD on your ``device`` with ``sh instantiate.sh``

When done with the instantiation, ``reboot`` your machine as usual.

