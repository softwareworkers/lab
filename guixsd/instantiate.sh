#!/bin/sh

echo "Instantiating GuixSD in /mnt"

# let packages be written to target
herd start cow-store /mnt

# copy the desired configuration
mkdir /mnt/etc
cp configuration/bare-bones.scm /mnt/etc/config.scm

# copy the desired ssh pub keys
cp -R ssh-authorized-keys /mnt/etc/

# let's install
guix system init /mnt/etc/config.scm /mnt

# disabled: for now you must do this explicitly
#reboot
