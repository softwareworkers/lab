#!/bin/sh

device="/dev/sda"

echo "Partitioning ${device} (ext4) for GuixSD install"

# partitioning with a MBR partition table
parted ${device} -- mklabel msdos
parted ${device} -- mkpart primary 1MiB -2GiB
parted ${device} -- mkpart primary linux-swap -2GiB 100%  # 2GB swap space

# format second partition as swap
mkswap -L swap ${device}2
swapon ${device}2

# format destination partition as ext4 FS and mount it
mkfs.ext4 -L guixsd ${device}1
mount -t ext4 ${device}1 /mnt/
