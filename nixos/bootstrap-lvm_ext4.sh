#!/bin/sh

device="/dev/sda"

# simple installation using btrfs using the whole disk device
# and subvolumes for root, var, home and tmp
echo "Bootstrapping on ${device} as LVM device"

# partitioning with a MBR partition table
parted ${device} -- mklabel msdos
parted ${device} -- mkpart primary 1MiB -2GiB
parted ${device} -- mkpart primary linux-swap -2GiB 100%  # 2GB swap space

# format second partition as swap
mkswap -L swap ${device}2
swapon ${device}2

# create physical and logical volumes
pvcreate -f ${device}1
vgcreate vg0 ${device}1
lvcreate --name nixos -L 10G vg0

# format destination volume as etx4 FS and mount it
mkfs.ext4 -L nixos /dev/vg0/nixos
mount -t ext4 /dev/vg0/nixos /mnt/

# let's finally generate the basic nixos config
nixos-generate-config --root /mnt/

# maintain generated config as .orig, use teh config shipped in this repo
mv /mnt/etc/nixos/configuration.nix /mnt/etc/nixos/configuration.nix.orig
cp configuration.nix /mnt/etc/nixos/configuration.nix
cp ssh-keys.nix /mnt/etc/nixos

# disabled: for now you must do this explicitly
#nixos-install
#reboot
