;; This is an operating system configuration template
;; for a "bare bones" setup, with no X11 display server.

(use-modules (gnu))
(use-service-modules networking ssh)
(use-package-modules screen ssh)

(operating-system
  (host-name "guixsd-test")
  (timezone "Europe/Rome")
  (locale "en_US.utf8")

  ;; Boot in "legacy" BIOS mode
  (bootloader (bootloader-configuration
                (bootloader grub-bootloader)
                (target "/dev/sda")))

  (file-systems (cons (file-system
                        (device (file-system-label "guixsd"))
                        (mount-point "/")
                        (type "ext4"))
                      %base-file-systems))

  ;; * TODO swap-devices ();

  ;; This is where user accounts are specified.  The "root"
  ;; account is implicit, and is initially created with the
  ;; empty password.
  (users (cons (user-account
                (name "gbiscuolo")
                (comment "Giovanni Biscuolo (admin)")
                (group "users")
                (supplementary-groups '("wheel" "audio" "video"))
                (home-directory "/home/gbiscuolo"))
               %base-user-accounts))

  ;; Globally-installed packages.
  (packages (cons* screen openssh %base-packages))

  ;; Add services to the baseline: a DHCP client and
  ;; an SSH server.
  (services (cons* (dhcp-client-service)
		   (service openssh-service-type
                            (openssh-configuration
			     (x11-forwarding? #t)
                             (permit-root-login 'without-password)
                             (authorized-keys
                              `(("gbiscuolo" ,(local-file "ssh-authorized-keys/gbiscuolo.pub"))))
                             (port-number 22)))
                   %base-services)))
